<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Creating-RST_POST</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="shortcut icon" href="https://download.trstone.com/productsupport/styles/favicon.ico" type="image/x-icon">
    </head>
    <body>
		<div id ="wrapper">
			<div id="header">
				<h1>RSM2 SSO POST Builder:</h1>
			</div><!-- #header -->
			<div id="content">
				<h3>Parameter List:</h3>
				<form id="post-form" method="POST" action="RSM2_SSO_POST.php">
					<div>
						<label for="client_id"><strong><font color="blue">client_id:</font></strong></label>
						<input type="text" required id="client_id" name="client_id" size="27" placeholder="Required parameter"/><a><font color="red"> **</font></a>
					</div>
					</br>
					<div>
						<label for="client_secret"><strong><font color="blue">client_secret:</font></strong></label>
						<input type="text" required id="client_secret" name="client_secret" placeholder="Required parameter"/><a><font color="red"> **</font></a>
					</div>
					</br>
					<div>
						<label for="salesPackageId"><strong><font color="blue">salesPackageId:</font></strong></label>
						<input type="text"  required id="salesPackageId" name="salesPackageId" size="41" placeholder="Required parameter"/><a><font color="red"> **</font></a>
					</div>
					</br>
					<div>
						<label for="username"><strong><font color="blue">username:</font></strong></label>
						<input type="text"  required id="username" name="username" placeholder="Required parameter"/><a><font color="red"> **</font></a>
					</div>
					</br>
					<div>
						<label for="targetLanguage"><strong>targetLanguage:</strong></label>
						<input type="text" id="targetLanguage" name="targetLanguage" size="25" /><a><font color="blue">!!</font></a>
					</div>
					</br>
					<div>
						<label for="firstName"><strong>firstName:</strong></label>
						<input type="text" id="firstName" name="firstName" size="25" />
					</div>
					</br>
					<div>
						<label for="lastName"><strong>lastName:</strong></label>
						<input type="text" id="lastName" name="lastName"  size="25"/>
					</div>
					</br>
					<div>
						<label for="email"><strong>email:</strong></label>
						<input type="email" id="email" name="email" size="30" placeholder="Enter valid email format!" /><a><font color="red"></font></a>
					</div>
					</br>
					<div>
						<label for="localization"><strong>localization:</strong></label>
						<input type="text" id="localization" name="localization"  size="5"/><a> Options:</a><font color="gray"><a>en-US, de-DE, es-419, fr-FR, it-IT, pt-BR, ja-JP, ko-KR</font></a>
					</div>
					</br>
					<div>
						<label for="timezone"><strong>timezone:</strong></label>
						<input type="text" id="timezone" name="timezone"  size="23"/>
					</div>
					</br>
					<div>
						<label for="gender"><strong>gender: </strong></label>
						<input type="radio" id="gender" name="gender" value="male" />Male
						<input type="radio" id="gender" name="gender" value="female"/>Female
					</div>
					<div>
						<label for="update"><strong>update: </strong></label>
						<input type="checkbox" id="update" name="update" value="true" />
					</div>
					<div>
						<label for="checksum"><strong>checksum:</strong></label>
						<input type="checkbox" id="checksum" name="checksum" value="true" />
					</div>
					<p><strong><font color="red">** These</font><font color="blue"> Parameters</font><font color="red"> are required!</font></strong></p>
					<p><strong><font color="blue">!!</font> If you don't assing this paramater the user auto-created with this post will be assigned an arbitrary language when used with Catalyst Platform.</strong></p>
					<div>
					<?php
					$footer = "Version 3.9.1 | 12.02.2016 | ";
					$footing = "<input name=\"footer\" value=\"$footer\" type=\"hidden\" />";
					print($footing);
					?>
						<input type="submit" name="submit" value="Create Post" />
					</div>
				</form>
			</div>
			<div id="footer">
				<footer><?php print ("<p><font color=\"green\">$footer Created by Hugo Mejia | Property of Rosetta Stone&reg;.</font></p>");?></footer>
			</div>
		</div>
    </body>
</html>
