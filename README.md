# README #

A Rosetta Stone RSM2 SSO POST Builder using PHP

### What is this repository for? ###

* Display RSM2 SSO usage of parameters
* Test out RSM2 SSO Mechanism
* Usage of checksum to use OAuth 1.0 1 legged secured AEB SSO POST 

### How do I get set up? ###

* Drop entire content of repo in a public folder within a Webserver with php deployed


### Who do I talk to? ###

* Hugo Mejia --> hmejia@rosettastone.com
* Rosetta Stone Technical Solutions team