<html>
    <head>
        <meta charset="UTF-8">
        <title>RSM2-SS0 POST</title>
		<!--<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>	-->
		<link rel="stylesheet" type="text/css" href="style.css" />
		<link rel="shortcut icon" href="https://download.trstone.com/productsupport/styles/favicon.ico" type="image/x-icon">
    </head>
    <body>
		<div id="wrapper">
			<div id="content">
			<?php
			//error_reporting(0);
			$checksumer = '';
			$updated = '';
			$button = '';
			$footer = '';
			if (!empty($_POST)){
				if(!empty($_POST['client_id']) && !empty($_POST['client_secret']) && !empty($_POST['username']) && !empty($_POST['salesPackageId'])){
						echo "<div id=\"header\"><h1>RSM2 SSO:</h1><h3>Please review the values assigned to the parameters you are including in this POST. Then click \"Submit\":</h3></div><hr>";
						$client_secret = $_POST['client_secret'];//will be given by Rosetta Stone.
						$client_id = $_POST['client_id'];  //will be given by Rosetta Stone.
						$salesPackageId = $_POST['salesPackageId'];
						$username = $_POST['username'];
						if (!empty($_POST['checksum'])){
							// the point of the following script is to generate a checksum = username+nonce+timestamp+client_secrete
							// the following for loop will create a random value to be used as nonce each time
							//the generated 12 character value would never have the same character to appear beside itself for added security.
							for ($i = 0, $z = strlen($a = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890')-1, $nonce = $a{rand(0,$z)}, $i = 1; $i != 12; $x = rand(0,$z), $nonce .= $a{$x}, $nonce = ($nonce{$i} == $nonce{$i-1} ? substr($nonce,0,-1) : $nonce), $i=strlen($nonce));
							$time=(string)gmdate("U"); //string is a prebuild php function that pulls current time
							$raw_vars = "$username$nonce$time$client_secret";
							$checksum = hash('sha256',$raw_vars);
							echo "<div id=\"params\"><strong>List of parameters that would be send with the HTTPS POST:</strong></br></br><strong>grant_type:</strong> client_asserted</br><strong>client_id:</strong> $client_id</br><strong>username:</strong> $username</br><strong>salesPackageId:</strong> $salesPackageId</br><strong>nonce:</strong> $nonce</br><strong>timestamp</strong>:$time</br><strong>checksum:</strong> $checksum</br>";
							$checksumer = "<input name='nonce' value=\"$nonce\" type='hidden' /><input name='timestamp' value=\"$time\" type='hidden' /><input name='checksum' value=\"$checksum\" type='hidden' />";
							$checksuming = "&amp;nonce=$nonce&amp;timestamp=$time&amp;checksum=$checksum";
							$cheksumnotes = "<li><strong>client_secret</strong> (\"<font color=\"red\">$client_secret</font>\") is only used to calculate \"checksum\" value and will not be used nor send in the POST.</li><li>Values use to calculate the checksum included in the POST was:<strong>\"$raw_vars\"</strong></li><li><strong><font color=\"green\">What is the value of the checksum in RSM2 SSO POST?</font></strong><br> Is a Sha256 hash of the string concatenation of the 4 parameters above (<font color=\"blue\">username</font>nonce<font color=\"blue\">timestamp</font>client_secret)</li></ul>"; 
						}else{
							$postclient_secret = $client_secret;
							echo "<div><strong>List of parameters that would be send with the HTTPS POST:</strong></br></br><strong>grant_type:</strong> client_asserted</br><strong>client_id:</strong> $client_id</br><strong>username:</strong> $username</br><strong>salesPackageId:</strong> $salesPackageId</br><strong>client_secret:</strong> $postclient_secret</br>";
							$checksumer = "<input name='client_secret' value=\"$postclient_secret\" type='hidden' />";
							$checksuming = "&amp;client_secret=$postclient_secret";
							$cheksumnotes = "<li><strong>client_secret</strong> (\"<font color=\"red\">$client_secret</font>\") will be sent with the post when <strong>\"checksum\"</strong> is not used.</li></ul>"; 
							}
						if (!empty($targetLanguage = $_POST['targetLanguage'])){
							$targetLanguage = $_POST['targetLanguage'];
							echo "<strong>targetLanguage:</strong>$targetLanguage</br>";
							$targetLanguageing = "&amp;targetLanguage=$targetLanguage";
						}else{
							$targetLanguageing = "";
						}
						if (!empty($_POST['firstName'])){
							$firstName = $_POST['firstName'];
							echo "<strong>firstName:</strong>$firstName</br>";
							$firstNaming = "&amp;firstName=$firstName";
						}else{
							$firstNaming = "";
							}
						if (!empty($_POST['lastName'])){
							$lastName =  $_POST['lastName'];
							echo "<strong>lastName:</strong> $lastName</br>";
							$lastNaming = "&amp;lastName=$lastName";
						}else{
							$lastNaming = "";
							}
						if (!empty($email = $_POST['email'])){
							$email = $_POST['email'];
							echo "<strong>email:</strong>$email</br>";
							$emailing = "&amp;email=$email";
						}else{
							$emailing = "";
						}
						if (!empty($_POST['localization'])){
							$localization =  $_POST['localization'];
							echo "<strong>localization:</strong> $localization</br>";
							$locale = "&amp;localization=$localization";
						}else{
							$locale = "";
							}
						if (!empty($_POST['gender'])){
							$gender = $_POST['gender'];
							echo "<strong>gender:</strong> $gender</br>";
							$gendering = "&amp;gender=$gender";						
						}else{
							$gendering = "";
							}
						if (!empty($_POST['update'])){
							$update = "true";
							echo "<strong>update:</strong> true</br>";
							$updated = "<input name='update' value=\"true\" type='hidden' />";
							$updating= "&amp;update=true";
						}else{
							$update = "false";
							echo "</br>";
							$updated = "";
							$updating= "";
							}
						if (!empty($_POST['timezone'])){
							$timezone =  $_POST['timezone'];
							echo "<strong>timezone:</strong> $timezone</br>";
							$timezoning = "&amp;timezone=$timezone";
						}else{
							$timezoning = "";
							}
						$button ="<hr><br><input value='Submit' type='submit' />";
						}else{
							echo "</div><h1>you must enter the **Required Parameters in the form, please try again.</h1>";
						}
				}else{
						echo "</br></br><h1><font color=\"red\">ACCESS DENIED</font></h1><h3>This page is not supposed to be accessed directly</h3>";
					}
			?>
				<form method='POST' action='https://tully.rosettastone.com/oauth/token'>
							<input name='grant_type' value='client_asserted' type='hidden' />
							<input name='client_id' value="<?php echo$client_id;?>" type='hidden' />
							<input name='username' value="<?php echo$username;?>" type='hidden' />
							<?php echo $checksumer;?>
							<input name='targetLanguage' value="<?php echo$targetLanguage;?>" type='hidden' />
							<input name='salesPackageId' value="<?php echo$salesPackageId;?>" type='hidden' />
							<input name='firstName' value="<?php echo$firstName;?>" type='hidden' />
							<input name='lastName' value="<?php echo$lastName;?>" type='hidden' />
							<input name='email' value="<?php echo$email;?>" type='hidden' />
							<input name='gender' value="<?php echo$gender;?>" type='hidden' />
							<input name='localization' value="<?php echo$localization;?>" type='hidden' />
							<input name='timezone' value="<?php echo$timezone;?>" type='hidden' />
							<?php echo $updated;echo $button;?>
				</form>
			<?php
			if (!empty($_POST)){
				if(!empty($_POST['client_id']) && !empty($_POST['client_secret']) && !empty($_POST['username']) && !empty($_POST['salesPackageId'])){
					$stringy = "<div id=\"stringy\"><p><a target=\"_blank\" href=\"https://tully.rosettastone.com/oauth/token?grant_type=client_asserted&client_id=$client_id&amp;username=$username$checksuming&amp;salesPackageId=$salesPackageId$targetLanguageing$firstNaming$lastNaming$emailing$locale$gendering$updating$timezoning\">https://tully.rosettastone.com/oauth/token?grant_type=client_asserted&client_id=$client_id&amp;username=$username$checksuming&amp;salesPackageId=$salesPackageId$targetLanguageing$firstNaming$lastNaming$emailing$locale$gendering$updating$timezoning</a></p></stringy>";
					print ($stringy);
					echo "<strong>Important Notes</strong>:</br><ul><li>Our system will only take HTTPS requests.</li>$cheksumnotes";}
					//$footer = $_POST['footer'];
					$footer = 'Version 3.9.1 | 12.02.2016 | ';
			}else{
					$footer = '';
			}
			?>
		</div>
			<div id="footer">
				<footer><?php print ("<p><font color=\"green\">$footer Created by Hugo Mejia | Property of Rosetta Stone&reg;.</font></p>");?></footer>
			</div><!-- #footer -->
		</div><!-- #wrapper -->
	</body>
</html>

